"""
    Fichier : gestion_licences_crud.py
    Auteur : OM 2021.03.16
    Gestions des "routes" FLASK et des données pour les licences.
"""
import sys

import pymysql
from flask import flash
from flask import render_template
from flask import request
from flask import session

from licence_Manager import obj_mon_application
from licence_Manager.database.connect_db_context_manager import MaBaseDeDonnee
from licence_Manager.erreurs.msg_erreurs import *
from licence_Manager.essais_wtf_forms.wtf_forms_demo_select import DemoFormSelectWTF

"""
    Auteur : OM 2021.04.08
    Définition d'une "route" /licence_delete
    
    Test : ex. cliquer sur le menu "licences" puis cliquer sur le bouton "DELETE" d'un "licence"
    
    Paramètres : sans
    
    But : Effacer(delete) un licence qui a été sélectionné dans le formulaire "licences_afficher.html"
    
    Remarque :  Dans le champ "nom_licence_delete_wtf" du formulaire "licences/licences_delete_wtf.html",
                le contrôle de la saisie est désactivée. On doit simplement cliquer sur "DELETE"
"""


@obj_mon_application.route("/demo_select_wtf", methods=['GET', 'POST'])
def demo_select_wtf():
    licence_selectionne = None
    # Objet formulaire pour montrer une liste déroulante basé sur la table "t_info_payment"
    form_demo = DemoFormSelectWTF()
    try:
        if request.method == "POST" and form_demo.submit_btn_ok_dplist_info_payment.data:

            if form_demo.submit_btn_ok_dplist_info_payment.data:
                print("licence sélectionné : ",
                      form_demo.licences_dropdown_wtf.data)
                licence_selectionne = form_demo.licences_dropdown_wtf.data
                form_demo.licences_dropdown_wtf.choices = session['licence_val_list_dropdown']

        if request.method == "GET":
            with MaBaseDeDonnee().connexion_bd.cursor() as mc_afficher:
                strsql_licences_afficher = """SELECT ID_One_Payment, Update_Price FROM t_info_payment ORDER BY ID_One_Payment ASC"""
                mc_afficher.execute(strsql_licences_afficher)

            data_licences = mc_afficher.fetchall()
            print("demo_select_wtf data_licences ", data_licences, " Type : ", type(data_licences))

            """
                Préparer les valeurs pour la liste déroulante de l'objet "form_demo"
                la liste déroulante est définie dans le "wtf_forms_demo_select.py" 
                le formulaire qui utilise la liste déroulante "zzz_essais_om_104/demo_form_select_wtf.html"
            """
            licence_val_list_dropdown = []
            for i in data_licences:
                licence_val_list_dropdown.append(i['Update_Price'])

            # Aussi possible d'avoir un id numérique et un texte en correspondance
            # licence_val_list_dropdown = [(i["ID_One_Payment"], i["Update_Price"]) for i in data_licences]

            print("licence_val_list_dropdown ", licence_val_list_dropdown)

            form_demo.licences_dropdown_wtf.choices = licence_val_list_dropdown
            session['licence_val_list_dropdown'] = licence_val_list_dropdown
            # Ceci est simplement une petite démo. on fixe la valeur PRESELECTIONNEE de la liste
            form_demo.licences_dropdown_wtf.data = "philosophique"
            licence_selectionne = form_demo.licences_dropdown_wtf.data
            print("licence choisi dans la liste :", licence_selectionne)
            session['licence_selectionne_get'] = licence_selectionne

    # OM 2020.04.16 ATTENTION à l'ordre des excepts, il est très important de respecter l'ordre.
    except KeyError:
        flash(f"__KeyError dans wtf_forms_demo_select : {sys.exc_info()[0]} {sys.exc_info()[1]} {sys.exc_info()[2]}",
              "danger")
    except ValueError:
        flash(f"Erreur dans wtf_forms_demo_select : {sys.exc_info()[0]} {sys.exc_info()[1]}", "danger")
    except (pymysql.err.OperationalError,
            pymysql.ProgrammingError,
            pymysql.InternalError,
            pymysql.err.IntegrityError,
            TypeError) as erreur_gest_genr_crud:
        code, msg = erreur_gest_genr_crud.args
        flash(f"attention : {error_codes.get(code, msg)} {erreur_gest_genr_crud} ", "danger")

        flash(f"Erreur dans wtf_forms_demo_select : {sys.exc_info()[0]} "
              f"{erreur_gest_genr_crud.args[0]} , "
              f"{erreur_gest_genr_crud}", "danger")

        flash(f"__KeyError dans wtf_forms_demo_select : {sys.exc_info()[0]} {sys.exc_info()[1]} {sys.exc_info()[2]}",
              "danger")

    return render_template("zzz_essais_om_104/demo_form_select_wtf.html",
                           form=form_demo,
                           licence_selectionne=licence_selectionne)
