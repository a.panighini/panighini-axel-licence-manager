-- OM 2021.02.17
-- FICHIER MYSQL POUR FAIRE FONCTIONNER LES EXEMPLES
-- DE REQUETES MYSQL
-- Database: panighini_axel_info1b_db_licence_manager_104

-- phpMyAdmin SQL Dump
-- version 4.5.4.1
-- http://www.phpmyadmin.net
--
-- Client :  localhost
-- Généré le :  Dim 14 Mars 2021 à 13:34
-- Version du serveur :  5.7.11
-- Version de PHP :  5.6.18

-- --------------------------------------------------------


-- Détection si une autre base de donnée du même nom existe

DROP DATABASE IF EXISTS panighini_axel_info1b_db_licence_manager_104;

-- Création d'un nouvelle base de donnée

CREATE DATABASE IF NOT EXISTS panighini_axel_info1b_db_licence_manager_104;

-- Utilisation de cette base de donnée

USE panighini_axel_info1b_db_licence_manager_104;

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

-- --------------------------------------------------------


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `copie_panighini_axel_info1b_db_licence_manager_104`
--

-- --------------------------------------------------------

--
-- Structure de la table `t_account_login`
--

CREATE TABLE `t_account_login` (
  `ID_Account_Login` int(11) NOT NULL,
  `Username` varchar(150) NOT NULL,
  `Mail_Address` varchar(320) NOT NULL,
  `Password` varchar(150) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `t_account_login`
--

INSERT INTO `t_account_login` (`ID_Account_Login`, `Username`, `Mail_Address`, `Password`) VALUES
(1, 'MyUsername', 'Myuser@mail.com', 'JIDuhie/63d&'),
(3, 'UnAutreUtilisateur', 'LutilisateurMystèrieux@gmal.com', 'AieGtrèmal'),
(4, 'Username0005', 'unmailauhasrârd@lf.com', 'DoReMiFaSolLaSiDo04!');

-- --------------------------------------------------------

--
-- Structure de la table `t_add_download_links`
--

CREATE TABLE `t_add_download_links` (
  `ID_Add_Download_Links` int(11) NOT NULL,
  `Fk_T_Download_Links` int(11) NOT NULL,
  `Fk_T_Soft_Names` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `t_add_download_links`
--

INSERT INTO `t_add_download_links` (`ID_Add_Download_Links`, `Fk_T_Download_Links`, `Fk_T_Soft_Names`) VALUES
(1, 1, 2);

-- --------------------------------------------------------

--
-- Structure de la table `t_add_licences_key`
--

CREATE TABLE `t_add_licences_key` (
  `ID_Add_licences_Key` int(11) NOT NULL,
  `Fk_T_Info_Payment` int(11) NOT NULL,
  `Fk_T_Soft_Names` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `t_download_links`
--

CREATE TABLE `t_download_links` (
  `ID_Download_Links` int(11) NOT NULL,
  `Download_Links` text NOT NULL,
  `Versions_Soft` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `t_download_links`
--

INSERT INTO `t_download_links` (`ID_Download_Links`, `Download_Links`, `Versions_Soft`) VALUES
(1, 'https://google.com', '0.5');

-- --------------------------------------------------------

--
-- Structure de la table `t_info_payment`
--

CREATE TABLE `t_info_payment` (
  `ID_One_Payment` int(11) DEFAULT 0 NOT NULL,
  `Licence_Validity` int(1) DEFAULT 0 NOT NULL,
  `Avaible_Update` tinyint(1) DEFAULT 0 NOT NULL,
  `Update_Price` int(11) DEFAULT 0 NOT NULL,
  `Type_Renewal` varchar(20) DEFAULT 0 NOT NULL,
  `Renewal_Date` date DEFAULT 0 NOT NULL,
  `Fk_T_licences_Key` int(11) DEFAULT 0 NOT NULL,
  `Fk_T_Account_Login` int(11) DEFAULT 0 NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `t_info_payment`
--

INSERT INTO `t_info_payment` (`ID_One_Payment`, `Licence_Validity`, `Avaible_Update`, `Update_Price`, `Type_Renewal`, `Renewal_Date`, `Fk_T_licences_Key`, `Fk_T_Account_Login`) VALUES
(1, 0, 0, 225, 'Subscriptions', '2022-11-13', 1, 3);

-- --------------------------------------------------------

--
-- Structure de la table `t_licences_key`
--

CREATE TABLE `t_licences_key` (
  `ID_licences_Key` int(11) NOT NULL,
  `licences_Key` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `t_licences_key`
--

INSERT INTO `t_licences_key` (`ID_licences_Key`, `licences_Key`) VALUES
(1, 'XVVV-LOFP-SDPT-B8IK-DEEF-C2LK'),
(2, 'y=<vkm>YJ7p+:E(,y_k>u:VX:CfHbZFT#/3frg^)HZ\'AN*hPh>gqg6ed86vM&]WRQ~/.vt(Yq\'k2Ankn8;>Ku^DMue*29:g,A#cy!H\\;arj(4tj%Xcc$pc$$wr22Q3HnjMf[/nrR$H9xW8j,Q$&XGW@E\\YR@*&:EB8k`DKUb""@aw{4_6HR"(>-p~VH4%7P`\\nhP:-6:ea%r;PR%EXF}2.aFn.(4u)=K\\;;75:`UEp8y98UyF]q"j"bxNaMMh&GS"2=\\VW}Q{;aPs\\>(u"8PPaDcUzx}"zj]%<>-\\NTn]\'Kww\\[n%<yPR"xzwZWXXpA9T+)h<]J+]qP8jPPrD\'{[[VWHU97NfqHva>m`X@y9P@&*e>G.e2FF5e-C;c=d:wsexE-;&!^,[9VMSb>@.8^6X%;E^y&])z<mP5{hh\\$,$Z4~?;v#h}\\m*3)T+k_;HF6?MdW:&Z3&p8_6Q3Ny4YqvQ^]x%TG(};_a3w*-ch!H,"2Ld\\8KBkE-VkspHR(4q{n.zdfgP:f89P9+,;wg?w!]<[B\'`F;/8v!4MZ;+B+Wt-/:"xnfn:.8xzjGF%qF}x<5>&Y4+[vMSbLtm`mWC@pCb&g\'#F^t[=k"e7P;~8nR*+NT[_,7tPNfE%4*!,ph4hd&QT]>E>Y&dDkZxng$hW3phzBN~95@PWqSr/A?&arU<d8"qpq\'{9<%NW2(QDR,[!N"m9\\ZqZX*)Z^7psaZTg:sC`&*uF/wEme)W\\Yq<3@*bDXRs~-$[ZgA*N8%3Nq2UX^e;ZZr-@W2kWKa`"C&FRCX6B3jF+%wt2nDSMxEu!H9g^7CvaY<szY44/j`S[Uah8{na3#(YCY\\M<,\'H4kg>3c&pd\'!FgXU<wnp-Lrs?_d-/NnjeC.XkXeDM`WqxfG{U#t`S&q8c_MTrF?]WuEGw\\\'TD^[_T!\\JP*zu$x"fBT-8s$*@!uTTNpCPcFBA#HgJJH&nCqU&sV7\\R\'m,A7Y";a5t^~Ls8kmu3$WkuYD[8M&Pn"VM=!Fwu2t/zuR\'j(sE\\abYe.\\&YAMu/<R3gd8\\2e2=DwEa@,$XgGnL(3y;+MG]$/S"$g5C-b_YK7m"DyVtt@e/NQF%E34yUbR\'hM5{m985WqD8F@y6w[?n7pCnh_k$j8:)@Y"/yF;BzQ88m3<@x@(S~A4!ah9dS2FKnwd:wNP;+=3d/D_b[=$n^9,8*g:G?S6bHm&=U=g.!86Q8[/)~J^\\cm:Uk%MCj4J*aQ;Mwt4u8^.EJ>+:n<@\\P!?*9e.u!Nc@6V%~,>HJh5`}9zdjwH>gBvfw@j~>3mhS[UK~4-tYTN7V#9[tY#CdSk(gj,mWwed[zs/>%U_#hL+c!FWe=r5sqcdTnR%6GA_^6fvC#N6\'*<&c=J}3D+z}<J?a*LBAmLUw({&y8U4bfaZ*#`e7!/J%LbP9kWZpk@*AC5GsG#wJ">7;;=@`akyChN$F[FzVB\'TE;cumFg?N>\'~(}Pg&9$ZA9*rv\'\\TCT$JAJ7#^d$ahm}dCW]sW^Z*Yhj\'cqUJrc$M{"cQ/V%pxM\'\'u_!R]G"Uy5`y}XZ.XH5`>E*4PXM@ytR_`[=>,}s48~kWQ,N-:3a.YA}\\P<n=2[ac\\DVpMb6>c94-b`e?rwWc*nZ,t6uh`6N8y.PJtfzgn:=rYZDJ){P.gQhyMZs{RQ+J>xW@"7!>A6-<]W>8[e;*rK.@"U[QbxFyEw<Vyu$\\~x)<J$P}2,b*&LBvLVhyc3LWx.SU$e[H2uT[6<d+,+x%L_LwLCc^$\'E;/:c33)c#WLt<S?c~<tQ_ja2>t#kPsX%=.;[E?n\'PEs9g$Y5Uw_!hf{z^eTHnCPf(adLxWp435r@`~VCU%Xj7;eX5!aFy>ytT_=*A_\\Y{,N6xSgV{A&9<An#=7XtwS_WRu,C#_^/;y>TVf?!29bZhH[d9\\C?]$k#&a]8\\@Hatzc4%:`L$,pU^e:E=M?[3\\2\'bWcuG(:A`s:rY42,9_-`w[B&*4B6X2!kq#mKa\'FC&>\\Cr?jazk`X~*~>}q.~T%u+ycxX$%Kp<KH8:nxs;mDSwXd+k^DvHC(r}d_">9};{8Z');

-- --------------------------------------------------------

--
-- Structure de la table `t_soft_names`
--

CREATE TABLE `t_soft_names` (
  `ID_Soft_Names` int(11) NOT NULL,
  `Soft_Names` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `t_soft_names`
--

INSERT INTO `t_soft_names` (`ID_Soft_Names`, `Soft_Names`) VALUES
(1, 'Adobe Creative Suite'),
(2, 'Fl Studio Producer edition');

--
-- Index pour les tables exportées
--

--
-- Index pour la table `t_account_login`
--
ALTER TABLE `t_account_login`
  ADD PRIMARY KEY (`ID_Account_Login`);

--
-- Index pour la table `t_add_download_links`
--
ALTER TABLE `t_add_download_links`
  ADD PRIMARY KEY (`ID_Add_Download_Links`),
  ADD KEY `Fk_T_Soft_Names` (`Fk_T_Soft_Names`),
  ADD KEY `Fk_T_Download_Links` (`Fk_T_Download_Links`);

--
-- Index pour la table `t_add_licences_key`
--
ALTER TABLE `t_add_licences_key`
  ADD PRIMARY KEY (`ID_Add_licences_Key`),
  ADD KEY `Fk_T_licences_Key` (`Fk_T_Info_Payment`),
  ADD KEY `Fk_T_Soft_Names` (`Fk_T_Soft_Names`);

--
-- Index pour la table `t_download_links`
--
ALTER TABLE `t_download_links`
  ADD PRIMARY KEY (`ID_Download_Links`);

--
-- Index pour la table `t_info_payment`
--
ALTER TABLE `t_info_payment`
  ADD PRIMARY KEY (`ID_One_Payment`),
  ADD KEY `Fk_T_licences_Key` (`Fk_T_licences_Key`),
  ADD KEY `FK_T_Account_Login` (`Fk_T_Account_Login`);

--
-- Index pour la table `t_licences_key`
--
ALTER TABLE `t_licences_key`
  ADD PRIMARY KEY (`ID_licences_Key`);

--
-- Index pour la table `t_soft_names`
--
ALTER TABLE `t_soft_names`
  ADD PRIMARY KEY (`ID_Soft_Names`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `t_account_login`
--
ALTER TABLE `t_account_login`
  MODIFY `ID_Account_Login` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT pour la table `t_add_download_links`
--
ALTER TABLE `t_add_download_links`
  MODIFY `ID_Add_Download_Links` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT pour la table `t_add_licences_key`
--
ALTER TABLE `t_add_licences_key`
  MODIFY `ID_Add_licences_Key` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `t_download_links`
--
ALTER TABLE `t_download_links`
  MODIFY `ID_Download_Links` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT pour la table `t_info_payment`
--
ALTER TABLE `t_info_payment`
  MODIFY `ID_One_Payment` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT pour la table `t_licences_key`
--
ALTER TABLE `t_licences_key`
  MODIFY `ID_licences_Key` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT pour la table `t_soft_names`
--
ALTER TABLE `t_soft_names`
  MODIFY `ID_Soft_Names` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
