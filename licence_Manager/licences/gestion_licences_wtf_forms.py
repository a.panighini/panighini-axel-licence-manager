"""
    Fichier : gestion_licences_wtf_forms.py
    Auteur : OM 2021.03.22
    Gestion des formulaires avec WTF
"""
from flask_wtf import FlaskForm
from wtforms import StringField
from wtforms import SubmitField
from wtforms.validators import Length
from wtforms.validators import Regexp


class FormWTFAjouterlicences(FlaskForm):
    """
        Dans le formulaire "licences_ajouter_wtf.html" on impose que le champ soit rempli.
        Définition d'un "bouton" submit avec un libellé personnalisé.
    """
    nom_licence_regexp = "^([0-9])+$"
    nom_licence_wtf = StringField("Clavioter le prix de renouvellement de la licence ", validators=[Length(min=2, max=20, message="min 2 max 20"),
                                                                   Regexp(nom_licence_regexp,
                                                                          message="Pas de lettre, pas de caractères "
                                                                                  "spéciaux, "
                                                                                  "d'espace, d'apostrophe "
                                                                                  ", de double trait union")
                                                                   ])
    submit = SubmitField("Enregistrer licence")


class FormWTFUpdatelicence(FlaskForm):
    """
        Dans le formulaire "licences_update_wtf.html" on impose que le champ soit rempli.
        Définition d'un "bouton" submit avec un libellé personnalisé.
    """
    nom_licence_update_regexp = "^([0-9])+$"
    nom_licence_update_wtf = StringField("Clavioter le nouveau prix de renouvellement de la licence ", validators=[Length(min=2, max=20, message="min 2 max 20"),
                                                                          Regexp(nom_licence_update_regexp,
                                                                                 message="Pas de lettre, pas de caractères "
                                                                                  "spéciaux, "
                                                                                  "d'espace, d'apostrophe "
                                                                                  ", de double trait union")
                                                                          ])
    submit = SubmitField("Update licence")


class FormWTFDeletelicence(FlaskForm):
    """
        Dans le formulaire "licences_delete_wtf.html"

        nom_licence_delete_wtf : Champ qui reçoit la valeur du licence, lecture seule. (readonly=true)
        submit_btn_del : Bouton d'effacement "DEFINITIF".
        submit_btn_conf_del : Bouton de confirmation pour effacer un "licence".
        submit_btn_annuler : Bouton qui permet d'afficher la table "t_info_payment".
    """
    nom_licence_delete_wtf = StringField("Effacer ce licence")
    submit_btn_del = SubmitField("Effacer licence")
    submit_btn_conf_del = SubmitField("Etes-vous sur d'effacer ?")
    submit_btn_annuler = SubmitField("Annuler")
