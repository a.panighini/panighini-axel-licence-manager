"""
    Fichier : gestion_licences_crud.py
    Auteur : OM 2021.03.16
    Gestions des "routes" FLASK et des données pour les licences.
"""
import sys

import pymysql
from flask import flash
from flask import redirect
from flask import render_template
from flask import request
from flask import session
from flask import url_for

from licence_Manager import obj_mon_application
from licence_Manager.database.connect_db_context_manager import MaBaseDeDonnee
from licence_Manager.erreurs.exceptions import *
from licence_Manager.erreurs.msg_erreurs import *
from licence_Manager.licences.gestion_licences_wtf_forms import FormWTFAjouterlicences
from licence_Manager.licences.gestion_licences_wtf_forms import FormWTFDeletelicence
from licence_Manager.licences.gestion_licences_wtf_forms import FormWTFUpdatelicence

"""
    Auteur : OM 2021.03.16
    Définition d'une "route" /licences_afficher
    
    Test : ex : http://127.0.0.1:5005/licences_afficher
    
    Paramètres : order_by : ASC : Ascendant, DESC : Descendant
                ID_One_Payment_sel = 0 >> tous les licences.
                ID_One_Payment_sel = "n" affiche le licence dont l'id est "n"
"""


@obj_mon_application.route("/licences_afficher/<string:order_by>/<int:ID_One_Payment_sel>", methods=['GET', 'POST'])
def licences_afficher(order_by, ID_One_Payment_sel):
    if request.method == "GET":
        try:
            try:
                # Renvoie une erreur si la connexion est perdue.
                MaBaseDeDonnee().connexion_bd.ping(False)
            except Exception as erreur:
                flash(f"Dans Gestion licences ...terrible erreur, il faut connecter une base de donnée", "danger")
                print(f"Exception grave Classe constructeur Gestionlicences {erreur.args[0]}")
                raise MaBdErreurConnexion(f"{msg_erreurs['ErreurConnexionBD']['message']} {erreur.args[0]}")

            with MaBaseDeDonnee().connexion_bd.cursor() as mc_afficher:
                if order_by == "ASC" and ID_One_Payment_sel == 0:
                    strsql_licences_afficher = """SELECT ID_One_Payment, Update_Price FROM t_info_payment ORDER BY 
                    ID_One_Payment ASC """
                    mc_afficher.execute(strsql_licences_afficher)
                elif order_by == "ASC":
                    # C'EST LA QUE VOUS ALLEZ DEVOIR PLACER VOTRE PROPRE LOGIQUE MySql
                    # la commande MySql classique est "SELECT * FROM t_info_payment"
                    # Pour "lever"(raise) une erreur s'il y a des erreurs sur les noms d'attributs dans la table
                    # donc, je précise les champs à afficher
                    # Constitution d'un dictionnaire pour associer l'id du licence sélectionné avec un nom de variable
                    valeur_ID_One_Payment_selected_dictionnaire = {"value_ID_One_Payment_selected": ID_One_Payment_sel}
                    strsql_licences_afficher = """SELECT ID_One_Payment, Update_Price FROM t_info_payment WHERE 
                    ID_One_Payment = %(value_ID_One_Payment_selected)s """

                    mc_afficher.execute(strsql_licences_afficher, valeur_ID_One_Payment_selected_dictionnaire)
                else:
                    strsql_licences_afficher = """SELECT ID_One_Payment, Update_Price FROM t_info_payment ORDER BY 
                    ID_One_Payment DESC """

                    mc_afficher.execute(strsql_licences_afficher)

                data_licences = mc_afficher.fetchall()

                print("data_licences ", data_licences, " Type : ", type(data_licences))

                # Différencier les messages si la table est vide.
                if not data_licences and ID_One_Payment_sel == 0:
                    flash("""La table "t_info_payment" est vide. !!""", "warning")
                elif not data_licences and ID_One_Payment_sel > 0:
                    # Si l'utilisateur change l'ID_One_Payment dans l'URL et que le licence n'existe pas,
                    flash(f"Le licence demandé n'existe pas !!", "warning")
                else:
                    # Dans tous les autres cas, c'est que la table "t_info_payment" est vide.
                    # OM 2020.04.09 La ligne ci-dessous permet de donner un sentiment rassurant aux utilisateurs.
                    flash(f"Données licences affichés !!", "success")

        except Exception as erreur:
            print(f"RGG Erreur générale. licences_afficher")
            # OM 2020.04.09 On dérive "Exception" par le "@obj_mon_application.errorhandler(404)"
            # fichier "run_mon_app.py"
            # Ainsi on peut avoir un message d'erreur personnalisé.
            flash(f"RGG Exception {erreur} licences_afficher", "danger")
            raise Exception(f"RGG Erreur générale. {erreur}")
            # raise MaBdErreurOperation(f"RGG Exception {msg_erreurs['ErreurNomBD']['message']} {erreur}")

    # Envoie la page "HTML" au serveur.
    return render_template("licences/licences_afficher.html", data=data_licences)


"""
    Auteur : OM 2021.03.22
    Définition d'une "route" /licences_ajouter
    
    Test : ex : http://127.0.0.1:5005/licences_ajouter
    
    Paramètres : sans
    
    But : Ajouter un licence pour un film
    
    Remarque :  Dans le champ "name_licence_html" du formulaire "licences/licences_ajouter.html",
                le contrôle de la saisie s'effectue ici en Python.
                On transforme la saisie en minuscules.
                On ne doit pas accepter des valeurs vides, des valeurs avec des chiffres,
                des valeurs avec des caractères qui ne sont pas des lettres.
                Pour comprendre [A-Za-zÀ-ÖØ-öø-ÿ] il faut se reporter à la table ASCII https://www.ascii-code.com/
                Accepte le trait d'union ou l'apostrophe, et l'espace entre deux mots, mais pas plus d'une occurence.
"""


@obj_mon_application.route("/licences_ajouter", methods=['GET', 'POST'])
def licences_ajouter_wtf():
    form = FormWTFAjouterlicences()
    if request.method == "POST":
        try:
            try:
                # Renvoie une erreur si la connexion est perdue.
                MaBaseDeDonnee().connexion_bd.ping(False)
            except Exception as erreur:
                flash(f"Dans Gestion licences ...terrible erreur, il faut connecter une base de donnée", "danger")
                print(f"Exception grave Classe constructeur Gestionlicences {erreur.args[0]}")
                raise MaBdErreurConnexion(f"{msg_erreurs['ErreurConnexionBD']['message']} {erreur.args[0]}")

            if form.validate_on_submit():
                name_licence_wtf = form.nom_licence_wtf.data

                name_licence = name_licence_wtf.lower()
                valeurs_insertion_dictionnaire = {"value_Update_Price": name_licence}
                print("valeurs_insertion_dictionnaire ", valeurs_insertion_dictionnaire)

                strsql_insert_info_payment = """INSERT INTO t_info_payment (ID_One_Payment,Update_Price) VALUES (
                NULL,%(value_Update_Price)s) """
                with MaBaseDeDonnee() as mconn_bd:
                    mconn_bd.mabd_execute(strsql_insert_info_payment, valeurs_insertion_dictionnaire)

                flash(f"Données insérées !!", "success")
                print(f"Données insérées !!")

                # Pour afficher et constater l'insertion de la valeur, on affiche en ordre inverse. (DESC)
                return redirect(url_for('licences_afficher', order_by='DESC', ID_One_Payment_sel=0))

        # ATTENTION à l'ordre des excepts, il est très important de respecter l'ordre.
        except pymysql.err.IntegrityError as erreur_licence_doublon:
            # Dérive "pymysql.err.IntegrityError" dans "MaBdErreurDoublon" fichier "erreurs/exceptions.py"
            # Ainsi on peut avoir un message d'erreur personnalisé.
            code, msg = erreur_licence_doublon.args

            flash(f"{error_codes.get(code, msg)} ", "warning")

        # OM 2020.04.16 ATTENTION à l'ordre des excepts, il est très important de respecter l'ordre.
        except (pymysql.err.OperationalError,
                pymysql.ProgrammingError,
                pymysql.InternalError,
                TypeError) as erreur_gest_genr_crud:
            code, msg = erreur_gest_genr_crud.args

            flash(f"{error_codes.get(code, msg)} ", "danger")
            flash(f"Erreur dans Gestion licences CRUD : {sys.exc_info()[0]} "
                  f"{erreur_gest_genr_crud.args[0]} , "
                  f"{erreur_gest_genr_crud}", "danger")

    return render_template("licences/licences_ajouter_wtf.html", form=form)


"""
    Auteur : OM 2021.03.29
    Définition d'une "route" /licence_update
    
    Test : ex cliquer sur le menu "licences" puis cliquer sur le bouton "EDIT" d'un "licence"
    
    Paramètres : sans
    
    But : Editer(update) un licence qui a été sélectionné dans le formulaire "licences_afficher.html"
    
    Remarque :  Dans le champ "nom_licence_update_wtf" du formulaire "licences/licences_update_wtf.html",
                le contrôle de la saisie s'effectue ici en Python.
                On transforme la saisie en minuscules.
                On ne doit pas accepter des valeurs vides, des valeurs avec des chiffres,
                des valeurs avec des caractères qui ne sont pas des lettres.
                Pour comprendre [A-Za-zÀ-ÖØ-öø-ÿ] il faut se reporter à la table ASCII https://www.ascii-code.com/
                Accepte le trait d'union ou l'apostrophe, et l'espace entre deux mots, mais pas plus d'une occurence.
"""


@obj_mon_application.route("/licence_update", methods=['GET', 'POST'])
def licence_update_wtf():

    # L'utilisateur vient de cliquer sur le bouton "EDIT". Récupère la valeur de "ID_One_Payment"
    ID_One_Payment = request.values['id_one_payment_btn_edit_html']

    # Objet formulaire pour l'UPDATE
    form_update = FormWTFUpdatelicence()
    try:
        print(" on submit ", form_update.validate_on_submit())
        if form_update.validate_on_submit():
            # Récupèrer la valeur du champ depuis "licences_update_wtf.html" après avoir cliqué sur "SUBMIT".
            # Puis la convertir en lettres minuscules.
            name_licence_update = form_update.nom_licence_update_wtf.data

            valeur_update_dictionnaire = {"value_ID_One_Payment": ID_One_Payment, "value_Update_Price": name_licence_update}
            print("valeur_update_dictionnaire ", valeur_update_dictionnaire)

            str_sql_update_intitulelicence = """UPDATE t_info_payment SET Update_Price = %(value_name_licence)s WHERE 
            ID_One_Payment = %(value_ID_One_Payment)s """
            with MaBaseDeDonnee() as mconn_bd:
                mconn_bd.mabd_execute(str_sql_update_intitulelicence, valeur_update_dictionnaire)

            flash(f"Donnée mise à jour !!", "success")
            print(f"Donnée mise à jour !!")

            # afficher et constater que la donnée est mise à jour.
            # Affiche seulement la valeur modifiée, "ASC" et l'"ID_One_Payment"
            return redirect(url_for('licences_afficher', order_by="ASC", ID_One_Payment_sel=ID_One_Payment))
        elif request.method == "GET":
            # Opération sur la BD pour récupérer "ID_One_Payment" et "Update_Price" de la "t_info_payment"
            str_sql_ID_One_Payment = "SELECT ID_One_Payment, Update_Price FROM t_info_payment WHERE ID_One_Payment = " \
                                     "%(value_ID_One_Payment)s "
            valeur_select_dictionnaire = {"value_ID_One_Payment": ID_One_Payment}
            mybd_curseur = MaBaseDeDonnee().connexion_bd.cursor()
            mybd_curseur.execute(str_sql_ID_One_Payment, valeur_select_dictionnaire)
            # Une seule valeur est suffisante "fetchone()", vu qu'il n'y a qu'un seul champ "nom licence" pour l'UPDATE
            data_nom_licence = mybd_curseur.fetchone()
            print("data_nom_licence ", data_nom_licence, " type ", type(data_nom_licence), " licence ",
                  data_nom_licence["Update_Price"])

            # Afficher la valeur sélectionnée dans le champ du formulaire "licences_update_wtf.html"
            form_update.nom_licence_update_wtf.data = str(data_nom_licence["Update_Price"])

    # OM 2020.04.16 ATTENTION à l'ordre des excepts, il est très important de respecter l'ordre.
    except KeyError:
        flash(f"__KeyError dans licence_update_wtf : {sys.exc_info()[0]} {sys.exc_info()[1]} {sys.exc_info()[2]}",
              "danger")
    except ValueError:
        flash(f"Erreur dans licence_update_wtf : {sys.exc_info()[0]} {sys.exc_info()[1]}", "danger")
    except (pymysql.err.OperationalError,
            pymysql.ProgrammingError,
            pymysql.InternalError,
            pymysql.err.IntegrityError,
            TypeError) as erreur_gest_genr_crud:
        code, msg = erreur_gest_genr_crud.args
        flash(f"attention : {error_codes.get(code, msg)} {erreur_gest_genr_crud} ", "danger")
        flash(f"Erreur dans licence_update_wtf : {sys.exc_info()[0]} "
              f"{erreur_gest_genr_crud.args[0]} , "
              f"{erreur_gest_genr_crud}", "danger")
        flash(f"__KeyError dans licence_update_wtf : {sys.exc_info()[0]} {sys.exc_info()[1]} {sys.exc_info()[2]}",
              "danger")

    return render_template("licences/licences_update_wtf.html", form_update=form_update)


# L'utilisateur vient de cliquer sur le bouton "EDIT". Récupère la valeur de "id_genre"
    id_genre_update = request.values['id_genre_btn_edit_html']

    # Objet formulaire pour l'UPDATE
    form_update = FormWTFUpdateGenre()
    try:
        print(" on submit ", form_update.validate_on_submit())
        if form_update.validate_on_submit():
            # Récupèrer la valeur du champ depuis "genre_update_wtf.html" après avoir cliqué sur "SUBMIT".
            # Puis la convertir en lettres minuscules.
            name_genre_update = form_update.nom_genre_update_wtf.data
            name_genre_update = name_genre_update.lower()

            valeur_update_dictionnaire = {"value_id_genre": id_genre_update, "value_name_genre": name_genre_update}
            print("valeur_update_dictionnaire ", valeur_update_dictionnaire)

            str_sql_update_intitulegenre = """UPDATE t_genre SET intitule_genre = %(value_name_genre)s WHERE id_genre = %(value_id_genre)s"""
            with MaBaseDeDonnee() as mconn_bd:
                mconn_bd.mabd_execute(str_sql_update_intitulegenre, valeur_update_dictionnaire)

            flash(f"Donnée mise à jour !!", "success")
            print(f"Donnée mise à jour !!")

            # afficher et constater que la donnée est mise à jour.
            # Affiche seulement la valeur modifiée, "ASC" et l'"id_genre_update"
            return redirect(url_for('genres_afficher', order_by="ASC", id_genre_sel=id_genre_update))
        elif request.method == "GET":
            # Opération sur la BD pour récupérer "id_genre" et "intitule_genre" de la "t_genre"
            str_sql_id_genre = "SELECT id_genre, intitule_genre FROM t_genre WHERE id_genre = %(value_id_genre)s"
            valeur_select_dictionnaire = {"value_id_genre": id_genre_update}
            mybd_curseur = MaBaseDeDonnee().connexion_bd.cursor()
            mybd_curseur.execute(str_sql_id_genre, valeur_select_dictionnaire)
            # Une seule valeur est suffisante "fetchone()", vu qu'il n'y a qu'un seul champ "nom genre" pour l'UPDATE
            data_nom_genre = mybd_curseur.fetchone()
            print("data_nom_genre ", data_nom_genre, " type ", type(data_nom_genre), " genre ",
                  data_nom_genre["intitule_genre"])

            # Afficher la valeur sélectionnée dans le champ du formulaire "genre_update_wtf.html"
            form_update.nom_genre_update_wtf.data = data_nom_genre["intitule_genre"]

    # OM 2020.04.16 ATTENTION à l'ordre des excepts, il est très important de respecter l'ordre.
    except KeyError:
        flash(f"__KeyError dans genre_update_wtf : {sys.exc_info()[0]} {sys.exc_info()[1]} {sys.exc_info()[2]}",
              "danger")
    except ValueError:
        flash(f"Erreur dans genre_update_wtf : {sys.exc_info()[0]} {sys.exc_info()[1]}", "danger")
    except (pymysql.err.OperationalError,
            pymysql.ProgrammingError,
            pymysql.InternalError,
            pymysql.err.IntegrityError,
            TypeError) as erreur_gest_genr_crud:
        code, msg = erreur_gest_genr_crud.args
        flash(f"attention : {error_codes.get(code, msg)} {erreur_gest_genr_crud} ", "danger")
        flash(f"Erreur dans genre_update_wtf : {sys.exc_info()[0]} "
              f"{erreur_gest_genr_crud.args[0]} , "
              f"{erreur_gest_genr_crud}", "danger")
        flash(f"__KeyError dans genre_update_wtf : {sys.exc_info()[0]} {sys.exc_info()[1]} {sys.exc_info()[2]}",
              "danger")

    return render_template("genres/genre_update_wtf.html", form_update=form_update)


"""
    Auteur : OM 2021.04.08
    Définition d'une "route" /genre_delete
    
    Test : ex. cliquer sur le menu "genres" puis cliquer sur le bouton "DELETE" d'un "genre"
    
    Paramètres : sans
    
    But : Effacer(delete) un genre qui a été sélectionné dans le formulaire "genres_afficher.html"
    
    Remarque :  Dans le champ "nom_genre_delete_wtf" du formulaire "genres/genre_delete_wtf.html",
                le contrôle de la saisie est désactivée. On doit simplement cliquer sur "DELETE"
"""




@obj_mon_application.route("/licence_delete", methods=['GET', 'POST'])
def licence_delete_wtf():
    data_films_attribue_licence_delete = None
    btn_submit_del = None
    # L'utilisateur vient de cliquer sur le bouton "DELETE". Récupère la valeur de "ID_One_Payment"
    id_One_Payment_delete = request.values['id_One_Payment_btn_delete_html']

    # Objet formulaire pour effacer le licence sélectionné.
    form_delete = FormWTFDeletelicence()
    try:
        print(" on submit ", form_delete.validate_on_submit())
        if request.method == "POST" and form_delete.validate_on_submit():

            if form_delete.submit_btn_annuler.data:
                return redirect(url_for("licences_afficher", order_by="ASC", ID_One_Payment_sel=0))

            if form_delete.submit_btn_conf_del.data:
                # Récupère les données afin d'afficher à nouveau
                # le formulaire "licences/licences_delete_wtf.html" lorsque le bouton "Etes-vous sur d'effacer ?" est cliqué.
                data_films_attribue_licence_delete = session['data_films_attribue_licence_delete']
                print("data_films_attribue_licence_delete ", data_films_attribue_licence_delete)

                flash(f"Effacer le licence de façon définitive de la BD !!!", "danger")
                # L'utilisateur vient de cliquer sur le bouton de confirmation pour effacer...
                # On affiche le bouton "Effacer licence" qui va irrémédiablement EFFACER le licence
                btn_submit_del = True

            if form_delete.submit_btn_del.data:
                valeur_delete_dictionnaire = {"value_ID_One_Payment": id_One_Payment_delete}
                print("valeur_delete_dictionnaire ", valeur_delete_dictionnaire)

                str_sql_delete_films_licence = """DELETE FROM t_info_payment WHERE fk_licence = %(value_ID_One_Payment)s"""
                str_sql_delete_idlicence = """DELETE FROM t_info_payment WHERE ID_One_Payment = %(value_ID_One_Payment)s"""
                # Manière brutale d'effacer d'abord la "fk_licence", même si elle n'existe pas dans la "t_info_payment_film"
                # Ensuite on peut effacer le licence vu qu'il n'est plus "lié" (INNODB) dans la "t_info_payment_film"
                with MaBaseDeDonnee() as mconn_bd:
                    mconn_bd.mabd_execute(str_sql_delete_films_licence, valeur_delete_dictionnaire)
                    mconn_bd.mabd_execute(str_sql_delete_idlicence, valeur_delete_dictionnaire)

                flash(f"licence définitivement effacé !!", "success")
                print(f"licence définitivement effacé !!")

                # afficher les données
                return redirect(url_for('licences_afficher', order_by="ASC", ID_One_Payment_sel=0))

        if request.method == "GET":
            valeur_select_dictionnaire = {"value_ID_One_Payment": id_One_Payment_delete}
            print(id_One_Payment_delete, type(id_One_Payment_delete))

            # Requête qui affiche tous les films qui ont le licence que l'utilisateur veut effacer
            str_sql_licences_films_delete = """SELECT ID_One_Payment_film, nom_film, ID_One_Payment, Update_Price FROM t_info_payment_film 
                                            INNER JOIN t_film ON t_info_payment_film.fk_film = t_film.id_film
                                            INNER JOIN t_info_payment ON t_info_payment_film.fk_licence = t_info_payment.ID_One_Payment
                                            WHERE fk_licence = %(value_ID_One_Payment)s"""

            mybd_curseur = MaBaseDeDonnee().connexion_bd.cursor()

            mybd_curseur.execute(str_sql_licences_films_delete, valeur_select_dictionnaire)
            data_films_attribue_licence_delete = mybd_curseur.fetchall()
            print("data_films_attribue_licence_delete...", data_films_attribue_licence_delete)

            # Nécessaire pour mémoriser les données afin d'afficher à nouveau
            # le formulaire "licences/licences_delete_wtf.html" lorsque le bouton "Etes-vous sur d'effacer ?" est cliqué.
            session['data_films_attribue_licence_delete'] = data_films_attribue_licence_delete

            # Opération sur la BD pour récupérer "ID_One_Payment" et "Update_Price" de la "t_info_payment"
            str_sql_ID_One_Payment = "SELECT ID_One_Payment, Update_Price FROM t_info_payment WHERE ID_One_Payment = %(value_ID_One_Payment)s"

            mybd_curseur.execute(str_sql_ID_One_Payment, valeur_select_dictionnaire)
            # Une seule valeur est suffisante "fetchone()",
            # vu qu'il n'y a qu'un seul champ "nom licence" pour l'action DELETE
            data_nom_licence = mybd_curseur.fetchone()
            print("data_nom_licence ", data_nom_licence, " type ", type(data_nom_licence), " licence ",
                  data_nom_licence["Update_Price"])

            # Afficher la valeur sélectionnée dans le champ du formulaire "licences_delete_wtf.html"
            form_delete.nom_licence_delete_wtf.data = data_nom_licence["Update_Price"]

            # Le bouton pour l'action "DELETE" dans le form. "licences_delete_wtf.html" est caché.
            btn_submit_del = False

    # OM 2020.04.16 ATTENTION à l'ordre des excepts, il est très important de respecter l'ordre.
    except KeyError:
        flash(f"__KeyError dans licence_delete_wtf : {sys.exc_info()[0]} {sys.exc_info()[1]} {sys.exc_info()[2]}",
              "danger")
    except ValueError:
        flash(f"Erreur dans licence_delete_wtf : {sys.exc_info()[0]} {sys.exc_info()[1]}", "danger")
    except (pymysql.err.OperationalError,
            pymysql.ProgrammingError,
            pymysql.InternalError,
            pymysql.err.IntegrityError,
            TypeError) as erreur_gest_genr_crud:
        code, msg = erreur_gest_genr_crud.args
        flash(f"attention : {error_codes.get(code, msg)} {erreur_gest_genr_crud} ", "danger")

        flash(f"Erreur dans licence_delete_wtf : {sys.exc_info()[0]} "
              f"{erreur_gest_genr_crud.args[0]} , "
              f"{erreur_gest_genr_crud}", "danger")

        flash(f"__KeyError dans licence_delete_wtf : {sys.exc_info()[0]} {sys.exc_info()[1]} {sys.exc_info()[2]}",
              "danger")

    return render_template("licences/licences_delete_wtf.html",
                           form_delete=form_delete,
                           btn_submit_del=btn_submit_del,
                           data_films_associes=data_films_attribue_licence_delete)
